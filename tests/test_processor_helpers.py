"""
Test processor helpers
"""
from unittest import TestCase, main

from json import load
from processor.processor_helpers import generate_hour, get_generator_with_remain


class HourGeneratorTest(TestCase):

    def setUp(self):
        with open('../fixtures/pdf_payload_example.json') as f:
            self.pdf_payload = load(f)

        with open('../fixtures/unsorted_payload_example.json') as f:
            self.unsorted_payload = load(f)

    def test_generate_hour_dictionaries(self):
        """
        generate_hour should generate the expected dictionaries
        """
        expected_dictionaries = [
            {'type': u'close', 'day': 'monday', 'value': 32},
            {'type': u'open', 'day': 'tuesday', 'value': 36000},
            {'type': u'close', 'day': 'tuesday', 'value': 64800},
            {'type': u'open', 'day': 'thursday', 'value': 36000},
            {'type': u'close', 'day': 'thursday', 'value': 64800},
            {'type': u'open', 'day': 'friday', 'value': 36000},
            {'type': u'close', 'day': 'saturday', 'value': 8000},
            {'type': u'open', 'day': 'saturday', 'value': 36000},
            {'type': u'close', 'day': 'sunday', 'value': 9000},
            {'type': u'open', 'day': 'sunday', 'value': 43200}
        ]

        for dictionary in generate_hour(self.unsorted_payload):
            expected_dictionary = expected_dictionaries.pop(0)
            self.assertDictEqual(expected_dictionary, dictionary)

    def test_get_generator_with_remain_with_remain(self):
        """
        get_generator_with_remain should generate the expected remain
        """

        _, remain = get_generator_with_remain(
            self.unsorted_payload)

        self.assertEqual(remain,
                         {'type': u'close', 'day': 'monday', 'value': 32})

    def test_get_generator_with_remain_with_remain_generator(self):
        """
        get_generator_with_remain should generate hour with it has a remain
        """

        hour_generator, _ = get_generator_with_remain(
            self.unsorted_payload)

        expected_generator = generate_hour(self.unsorted_payload)
        next(expected_generator)
        self.assertEqual(next(hour_generator),
                         next(expected_generator))

    def test_get_generator_with_remain_without_remain(self):
        """
        get_generator_with_remain should generate empty remain
        """

        _, remain = get_generator_with_remain(
            self.pdf_payload)

        self.assertIsNone(remain)

    def test_get_generator_with_remain_without_remain_generator(self):
        """
        get_generator_with_remain should generate expected hour without remain
        """

        hour_generator, _ = get_generator_with_remain(
            self.pdf_payload)

        expected_generator = generate_hour(self.pdf_payload)
        self.assertEqual(next(hour_generator),
                         next(expected_generator))


if __name__ == '__main__':
    main()
