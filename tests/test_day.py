"""
Test day class
"""
from unittest import TestCase, main

from processor.data_structures import Day, TimeFrame
from processor.exceptions import InvalidDayNameError, TimeFrameNotCompleteError
from utils import CLOSE_DICT, OPEN_DICT


class DayTest(TestCase):

    def test_day_initialisation_valid_name(self):
        """
        Check if day is initialised with a valid week day name
        """
        day = Day('tuesday')
        self.assertEqual(day.name, 'tuesday')

    def test_day_initialisation_invalid_name(self):
        """
        Check if day is initialised with a valid week day name
        """
        with self.assertRaises(InvalidDayNameError):
            Day('random')

    def test_day_add_complete_time_frame(self):
        """
        Check if a complete name is added to the day's list
        """
        time_frame = TimeFrame(OPEN_DICT)
        time_frame.set_close_time(CLOSE_DICT)

        day = Day('monday')
        day.add_open_time_frame(time_frame)

        self.assertEqual(time_frame, day.time_frames[0])

    def test_day_add_incomplete_time_frame(self):
        """
        Check if an incomplete name fails to be added to the day's list
        """
        time_frame = TimeFrame(OPEN_DICT)

        day = Day('monday')
        with self.assertRaises(TimeFrameNotCompleteError):
            day.add_open_time_frame(time_frame)

    def test_day_string_with_time_frame(self):
        """
        Conversion from day to string should be according with the expected
        """
        time_frame = TimeFrame(OPEN_DICT)
        time_frame.set_close_time(CLOSE_DICT)
        day = Day('monday')
        day.add_open_time_frame(time_frame)
        self.assertEqual("Monday: 09 AM - 02 PM",
                         str(day))

    def test_day_string_closed(self):
        """
        Conversion from day to string should be according with the expected
        for closed day
        """
        day = Day('monday')
        self.assertEqual("Monday: Closed",
                         str(day))


if __name__ == '__main__':
    main()
