"""
File contains tests for OpenTimeProcessor methods
"""
from json import load
from unittest import TestCase, main

from processor.data_structures import Day
from processor.exceptions import (
    InvalidValueError,
    OpenTimeProcessorValidationError,
    OpenTimeProcessorError
)
from processor.openings_processor import OpenTimeProcessor


class OpenTimeProcessorAuxiliaryMethodsTest(TestCase):
    """
    Class to test auxiliary methods for OpenTimeProcessor
    """

    def setUp(self):
        self.open_time_processor = OpenTimeProcessor()

    def test_get_collection_day_not_initialised(self):
        """
        In _get_collection_day, if day was not initialised a instance should be
         returned with the current day name.
        """
        current_day_name = 'monday'

        collection_day = self.open_time_processor._get_collection_day(
            None, current_day_name, [])

        self.assertEqual(collection_day.name, current_day_name)

    def test_get_collection_day_same_day(self):
        """
        In _get_collection_day, if the current day name is equal to the name
        of the instance, the instance should be returned.
        """
        current_day_name = 'monday'
        day_instance = Day(current_day_name)

        collection_day = self.open_time_processor._get_collection_day(
            day_instance, current_day_name, [])

        self.assertEqual(day_instance, collection_day)

    def test_get_collection_day_different_returned_day(self):
        """
        In _get_collection_day, if the current day name is different from the
        name of the instance, a instance with the new name should be returned
        """
        current_day_name = 'tuesday'
        day_instance = Day('monday')
        day_list = []

        collection_day = self.open_time_processor._get_collection_day(
            day_instance, current_day_name, day_list)

        self.assertEqual(collection_day.name, current_day_name)

    def test_get_collection_day_different_day_list(self):
        """
        In _get_collection_day, if the current day name is different from the
        name of the instance, the instance should be added to the list of days
        """
        current_day_name = 'tuesday'
        day_instance = Day('monday')
        day_list = []

        self.open_time_processor._get_collection_day(
            day_instance, current_day_name, day_list)

        self.assertEqual(day_list[0], day_instance)

    def test__is_close_day_valid_same_day(self):
        """
        _is_close_day_valid should consider same day close valid
        """
        open_day = Day('monday')
        close_entry = {'day': 'monday'}

        try:
            self.open_time_processor._is_close_day_valid(open_day, close_entry)
        except OpenTimeProcessorError:
            self.fail('Same day close should be valid')

    def test__is_close_day_valid_next_day(self):
        """
        _is_close_day_valid should consider next day close valid
        """
        open_day = Day('sunday')
        close_entry = {'day': 'monday'}

        try:
            self.open_time_processor._is_close_day_valid(open_day, close_entry)
        except OpenTimeProcessorError:
            self.fail('Next day close should be valid')

    def test__is_close_day_valid_next_day_middle_week(self):
        """
        _is_close_day_valid should consider next day close valid
        """
        open_day = Day('wednesday')
        close_entry = {'day': 'thursday'}

        try:
            self.open_time_processor._is_close_day_valid(open_day, close_entry)
        except OpenTimeProcessorError:
            self.fail('Next day close should be valid')

    def test__is_close_day_valid_invalid(self):
        """
        _is_close_day_valid should raise exception for invalid days
        """
        open_day = Day('sunday')
        close_entry = {'day': 'friday'}

        with self.assertRaises(OpenTimeProcessorError):
            self.open_time_processor._is_close_day_valid(open_day, close_entry)


class OpenTimeProcessorTest(TestCase):
    """
    Contains tests to process open hours
    """

    def setUp(self):
        self.open_time_processor = OpenTimeProcessor()

    def _get_json_fixture(self, file_directory):
        with open(file_directory) as f:
            payload = load(f)
        return payload

    def test_invalid_value(self):
        """
        process weekly schedules with values greater than allowed should raise
        InvalidValueError
        """
        payload = self._get_json_fixture('../fixtures/'
                                         'invalid_value_example.json')
        with self.assertRaises(InvalidValueError):
            self.open_time_processor.process(payload)

    def test_incomplete_week_payload(self):
        """
        process weekly schedules with missing days of the week should raise
        OpenTimeProcessorValidationError
        """
        payload = self._get_json_fixture('../fixtures/'
                                         'incomplete_week_days_example.json')
        with self.assertRaises(OpenTimeProcessorValidationError):
            self.open_time_processor.process(payload)

    def test_close_without_open_payload(self):
        """
        process weekly schedule with missing open entry (but with the close entry)
        should raise OpenTimeProcessorError
        """
        payload = self._get_json_fixture('../fixtures/'
                                         'close_without_open_example.json')
        with self.assertRaises(OpenTimeProcessorError):
            self.open_time_processor.process(payload)

    def test_open_without_close_payload(self):
        """
        process weekly schedule with open entry without close entry
        should raise OpenTimeProcessorError
        """
        payload = self._get_json_fixture('../fixtures/'
                                         'open_without_close_example.json')
        with self.assertRaises(OpenTimeProcessorError):
            self.open_time_processor.process(payload)

    def test_close_time_separated_from_open_payload(self):
        """
        when open and close time have more than a day of distance, it
        should raise OpenTimeProcessorError
        """
        payload = self._get_json_fixture(
            '../fixtures/close_time_separated_from_open_example.json')
        with self.assertRaises(OpenTimeProcessorError):
            self.open_time_processor.process(payload)

    def test_unsorted_payload_example_str(self):
        """
        check if unsorted weekly schedule (with remain) returns the expected
        string
        """
        payload = self._get_json_fixture(
            '../fixtures/unsorted_payload_example.json')
        week = self.open_time_processor.process(payload)
        expected_week_str = """Monday: Closed
Tuesday: 10 AM - 06 PM
Wednesday: Closed
Thursday: 10 AM - 06 PM
Friday: 10 AM - 02 AM
Saturday: 10 AM - 02 AM
Sunday: 12 PM - 12 AM"""
        self.assertEqual(str(week), expected_week_str)

    def test_pdf_payload_example_str(self):
        """
        checks if the pdf example generates the expected string
        """
        payload = self._get_json_fixture(
            '../fixtures/pdf_payload_example.json')
        week = self.open_time_processor.process(payload)

        expected_week_str = """Monday: Closed
Tuesday: 10 AM - 06 PM
Wednesday: Closed
Thursday: 10 AM - 06 PM
Friday: 10 AM - 01 AM
Saturday: 10 AM - 01 AM
Sunday: 12 PM - 09 PM"""
        self.assertEqual(str(week), expected_week_str)


if __name__ == '__main__':
    main()
