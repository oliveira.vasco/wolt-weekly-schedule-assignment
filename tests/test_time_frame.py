"""
Test time frame class
"""
from unittest import TestCase, main

from processor.constants import MAX_VALUE
from processor.data_structures import TimeFrame
from processor.exceptions import (
    InvalidTypeError,
    InvalidValueError
)


class TimeFrameTest(TestCase):

    def setUp(self):
        self.default_open_dict = {
            'type': 'open',
            'value': 34532
        }
        self.default_close_dict = {
            'type': 'close',
            'value': 53453
        }

    def test_initialisation_valid_data(self):
        """
        Time frame should be initialised with the valid open data
        """
        time_frame = TimeFrame(self.default_open_dict)

        self.assertEqual(time_frame.start_time,
                         self.default_open_dict['value'])

    def test_initialisation_invalid_type(self):
        """
        Time frame should raise InvalidTypeError if the initialisation type
        is not open
        """

        with self.assertRaises(InvalidTypeError):
            TimeFrame(self.default_close_dict)

    def test_initialisation_invalid_value(self):
        """
        Time frame should raise InvalidValueError if the initialisation value
        is invalid
        """
        self.default_open_dict['value'] = MAX_VALUE + 1

        with self.assertRaises(InvalidValueError):
            TimeFrame(self.default_open_dict)

    def test_set_close_time_invalid_type(self):
        """
        Time frame should raise InvalidTypeError if set close type is not close
        """
        time_frame = TimeFrame(self.default_open_dict)
        with self.assertRaises(InvalidTypeError):
            time_frame.set_close_time(self.default_open_dict)

    def test_set_close_time_invalid_value(self):
        """
        Time frame should raise InvalidValueError if set close value is invalid
        """
        self.default_close_dict['value'] = MAX_VALUE + 1
        time_frame = TimeFrame(self.default_open_dict)
        with self.assertRaises(InvalidValueError):
            time_frame.set_close_time(self.default_close_dict)

    def test_complete_invalid_start_time(self):
        """
        A time frame with invalid start time is not complete
        """
        time_frame = TimeFrame(self.default_open_dict)
        time_frame.set_close_time(self.default_close_dict)

        time_frame.start_time = 100000
        self.assertFalse(time_frame.complete)

    def test_complete_invalid_end_time(self):
        """
        A time frame with invalid end time is not complete
        """
        time_frame = TimeFrame(self.default_open_dict)
        time_frame.set_close_time(self.default_close_dict)

        time_frame.end_time = 100000
        self.assertFalse(time_frame.complete)

    def test_complete_empty_start_time(self):
        """
        A time frame without start time is not complete
        """
        time_frame = TimeFrame(self.default_open_dict)
        time_frame.set_close_time(self.default_close_dict)
        time_frame.start_time = None
        self.assertFalse(time_frame.complete)

    def test_complete_empty_end_time(self):
        """
        A time frame without end time is not complete
        """
        time_frame = TimeFrame(self.default_open_dict)
        self.assertFalse(time_frame.complete)

    def test_complete_valid_time_frame(self):
        """
        A time frame without start time is not complete
        """
        time_frame = TimeFrame(self.default_open_dict)
        time_frame.set_close_time(self.default_close_dict)
        self.assertTrue(time_frame.complete)

    def test_time_frame_str(self):
        """
        Convert from time frame to string should be according the expected
        """
        time_frame = TimeFrame(self.default_open_dict)
        time_frame.set_close_time(self.default_close_dict)

        self.assertEqual(str(time_frame), '09 AM - 02 PM')


if __name__ == '__main__':
    main()
