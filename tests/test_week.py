"""
Test time frame class
"""
from unittest import TestCase, main

from processor.constants import WEEK_DAYS
from processor.data_structures import TimeFrame, Day, Week
from utils import CLOSE_DICT, OPEN_DICT


class WeekTest(TestCase):

    def setUp(self):
        time_frame = TimeFrame(OPEN_DICT)
        time_frame.set_close_time(CLOSE_DICT)

        monday = Day('monday')
        monday.add_open_time_frame(time_frame)
        friday = Day('friday')
        friday.add_open_time_frame(time_frame)

        collected_days_list = [monday, friday]

        self.week = Week(collected_days_list)

    def test_week_initialisation_missing_days(self):
        """
        Week class initialisation should add the missing days of the week
        """
        self.assertEqual(len(WEEK_DAYS),
                         len(self.week.days))

    def test_week_conversion_to_string(self):
        """
        Week conversion to string should be according the expected
        """
        self.assertEqual(str(self.week),
                         """Monday: 09 AM - 02 PM
Tuesday: Closed
Wednesday: Closed
Thursday: Closed
Friday: 09 AM - 02 PM
Saturday: Closed
Sunday: Closed""")


if __name__ == '__main__':
    main()
