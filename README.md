### Introduction

This application provides an API endpoint, with flask restful, to process open hours of restaurants.
In addition, it has an api client to generate requests from fixtures and print the response.

### Processor

The opening processor contains the logic of collection and validation.
It populates data structures while is processing the schedule.
Also, it uses validation functions in opening validator.

The processor helpers contains the generator and auxiliary functions to handle it.

Exceptions contains several custom exceptions used to segment the API responses.

#### Data structures

Three entities are used as structures.
First, time frame collects and validates the beginning and the end of an opening.
Second, day represents an aggregation of time frames.
Finally, week represents an aggregation of days.
All three of them have validation logic and methods to convert to string.
 