"""
Client script to test open hours endpoint
"""

from json import load
from requests import post

API_ENDPOINT = 'http://127.0.0.1:5000'

with open('fixtures/pdf_payload_example.json') as f:
    pdf_example = load(f)

response = post(API_ENDPOINT, json=pdf_example)

if response.ok:
    payload = response.json()
    print payload['message']
