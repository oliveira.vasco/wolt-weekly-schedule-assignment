"""
This file contains auxiliary functions for the OpenTimeProcessor
"""
from constants import (
    CLOSE,
    OPEN,
    WEEK_DAYS,
)
from processor.exceptions import OpenTimeProcessorError


def generate_hour(weekly_schedule):
    """
    Hour generator generates a dictionary for each open/close entry.
    For each day of the week, sorts the open/close by time and generates a dict
    with type, day and value
    :param weekly_schedule: dictionary with week days in the keys. Contains
    nested dictionaries in each value with the open/close hours.
    :return: generates a dictionary with the day, type and hour
    """
    for week_day in WEEK_DAYS:
        hours = weekly_schedule[week_day]
        sorted_hours = sorted(hours, key=lambda entry: entry['value'])
        for hour in sorted_hours:
            yield {'day': week_day, 'type': hour['type'], 'value': hour['value']}


def get_generator_with_remain(weekly_schedule):
    """
    This function checks the first element of the generator. In case it's a
    close, it sets it as remain (to be used in the end). In case it's open,
    it resets the generator and sets the remain as None
    :param weekly_schedule: dictionary with week days in the keys. Contains
    nested dictionaries in each value with the open/close hours.
    :return: tuple with the generator to use in the first element and the remain
    in the second (in case there's a remain, otherwise is None).
    """
    hour_generator = generate_hour(weekly_schedule)
    first_element = next(hour_generator)
    remain = None

    if first_element['type'] == CLOSE:
        remain = first_element
    elif first_element['type'] == OPEN:
        hour_generator = generate_hour(weekly_schedule)
    else:
        raise OpenTimeProcessorError('Type not supported')

    return hour_generator, remain
