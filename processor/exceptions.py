"""File contains custom exceptions"""


class OpenTimeProcessorError(Exception):
    """
    Generic logic error for the OpenTimeProcessor
    """


class OpenTimeProcessorValidationError(Exception):
    """
    Validation error for OpenTimeProcessor
    """


class InvalidTypeError(OpenTimeProcessorValidationError):
    """
    Invalid type error in case the type is not open or close
    """


class InvalidValueError(OpenTimeProcessorValidationError):
    """
    Invalid value error for values lower or higher them the admissible.
    """


class TimeFrameNotCompleteError(OpenTimeProcessorError):
    """
    Error raised when an incomplete time frame is added to a day object
    """


class InvalidDayNameError(OpenTimeProcessorValidationError):
    """
    Error raised when the name of the day provided is not a valid week day name
    """
