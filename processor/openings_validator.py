"""
Contains validation functions
"""
from constants import WEEK_DAYS
from processor.exceptions import OpenTimeProcessorValidationError


def _validate_weekly_schedule_type(weekly_schedule):
    """
    weekly schedule should be a dictionary
    """
    if not isinstance(weekly_schedule, dict):
        raise OpenTimeProcessorValidationError(
            'Weekly schedule is not a dictionary')


def _validate_week_days(weekly_schedule):
    """
    weekly schedule should contain all days of the week in its keys
    """
    if set(weekly_schedule.keys()) != set(WEEK_DAYS):
        raise OpenTimeProcessorValidationError(
            'Week days are not according the expected.')


validators = [_validate_weekly_schedule_type,
              _validate_week_days]


def validate_openings(weekly_schedule):
    """
    Runs all the validators funtions in validators list for the given weekly
    schedule
    :param weekly_schedule: dictionary with week days in the keys. Contains
    nested dictionaries in each value with the open/close hours.
    :return: raises OpenTimeProcessorValidationError if any validator fails
    """
    for validator in validators:
        validator(weekly_schedule)
