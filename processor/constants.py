"""
File contains constants
"""

OPEN = 'open'
CLOSE = 'close'
WEEK_DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday',
             'sunday']
MIN_VALUE = 0
MAX_VALUE = 86399
