"""
File contains time frame class and methods
"""

from processor.constants import (
    CLOSE,
    MAX_VALUE,
    MIN_VALUE,
    OPEN
)
from processor.exceptions import (
    InvalidTypeError,
    InvalidValueError
)
from processor.data_structures.utils import validate_equality

from time import gmtime, strftime


class TimeFrame(object):
    """
    Time frame objects models the opening frame of a restaurant.
    It has a start_time and end_time attributes.
    In addition, it performs some validation.
    """

    end_time = None

    def __init__(self, open_dict):
        """
        Constructor validates the received open dictionary and initializes the
        open time attribute
        :param open_dict: dictionary with type and value keys.
        """

        self._validate_type(open_dict['type'],
                                 OPEN,
                                 'Type is not open.')

        self._validate_value(open_dict['value'])

        self.start_time = open_dict['value']

    def set_close_time(self, close_dict):
        """
        Sets the close time from the given dictionary.
        In addition, type and value are validated.
        :param close_dict: dictionary with type and value keys
        """

        self._validate_type(close_dict['type'],
                                 CLOSE,
                                 'Type is not close.')

        self._validate_value(close_dict['value'])

        self.end_time = close_dict['value']

    def _validate_type(self, received_type, expected_type, error_message):
        """
        Validates if the received type is according the expected
        :param received_type: type received
        :param expected_type: type expected value
        :param error_message: error message to include in the exception
        :return: raises an InvalidTypeError if type is not valid
        """
        validate_equality(received_type, expected_type,
                          InvalidTypeError(error_message))

    def _validate_value(self, value):
        """
        Validates if the value is between the allowed boundaries
        :param value: value to validate
        :return: raises InvalidValueError if the value is not valid
        """
        if value < MIN_VALUE or value > MAX_VALUE:
            raise InvalidValueError(
                'Time value %s is out of the admissible range.' % value)

    @property
    def complete(self):
        """
        A TimeFrame instance is complete when both of the fields are valid
        and not None
        :return: True if the both values are not None and valid
        """
        try:
            self._validate_value(self.start_time)
            self._validate_value(self.end_time)
        except InvalidValueError:
            return False
        return bool(self.start_time and self.end_time)

    def __str__(self):
        """
        Conversion to string displays both the open and close time of the
        time frame
        :return: string with both start and end time
        """
        start_str = strftime("%I %p", gmtime(self.start_time))
        end_str = strftime("%I %p", gmtime(self.end_time))
        return '{start_str} - {end_str}'.format(start_str=start_str,
                                                end_str=end_str)
