"""
File contains day class and methods
"""
from processor.constants import WEEK_DAYS
from processor.exceptions import InvalidDayNameError, TimeFrameNotCompleteError


class Day(object):
    """
    Class models the logic of day opening hours.
    A day has a name (corresponds to the day of the week)
    Also, it contains a list of time frames
    """

    def __init__(self, name):
        """
        Constructor initializes an empty list of time frames and validates and
        collects the received name.
        :param name: day of the week's name
        """
        self.time_frames = []

        self.name = name
        self._validate_name()

    def _validate_name(self):
        """
        Name of the day should be a week day
        :return: raises an InvalidDayNameError if the name is not valid
        """
        if self.name not in WEEK_DAYS:
            raise InvalidDayNameError('Invalid day name')

    def add_open_time_frame(self, time_frame):
        """
        Adds a time frame to the list of time frames of the day.
        Only adds it if the time frame is complete
        :param time_frame: time frame to be added
        :return: raises TimeFrameNotCompleteError when the time frame
        is not complete
        """
        if not time_frame.complete:
            raise TimeFrameNotCompleteError(
                "Time frame provided is not complete.")
        self.time_frames.append(time_frame)

    def __str__(self):
        """
        String conversion of the object should aggregate all the time frames
        stored in a string.
        In case no time frames were added, the string closed should be included
        :return: string that summarizes the openings of the day
        """
        if self.time_frames:
            hours_string_list = []
            for time_frame in self.time_frames:
                hours_string_list.append(str(time_frame))
            hours_string = ','.join(hours_string_list)

        else:
            hours_string = 'Closed'

        day_string = '{day}: {hours}'.format(day=self.name.capitalize(),
                                             hours=hours_string)

        return day_string
