"""
File contains week class and its methods
"""
from processor.constants import WEEK_DAYS
from processor.data_structures.day import Day
from processor.data_structures.utils import pop_first_element


class Week(object):
    """
    Class models the week. The week class populates its elements with the given
    list. In addition, it fills the unexistent days, of the list, with empty
    days.
    """

    def __init__(self, week_days_list):
        """
        Constructor populates the attribute days with the week days.
        In case those days are already present in the given list, it populates
        the list with the given value. Otherwise, it populates it with an
        empty day.
        :param week_days_list: list of instances of Day class
        """
        self.days = []

        day_from_list = pop_first_element(week_days_list)

        for day_name in WEEK_DAYS:
            if day_from_list and day_name == day_from_list.name:
                self.days.append(day_from_list)
                day_from_list = pop_first_element(week_days_list)
            else:
                day_instance = Day(day_name)
                self.days.append(day_instance)

    def __str__(self):
        """
        String conversion joins the strings for each day and separates
        them with a break.
        :return:
        """
        week_str_list = []
        for day in self.days:
            week_str_list.append(str(day))
        return '\n'.join(week_str_list)
