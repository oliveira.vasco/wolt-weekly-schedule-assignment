"""
File contains generic auxiliary functions
"""


def pop_first_element(elements_list):
    """
    Pops the first element of a list
    :param elements_list: list of elements
    :return: the first element of the list.
    The element is removed from the original list.
    """
    if elements_list:
        element = elements_list.pop(0)
    else:
        element = None

    return element


def validate_equality(received, expected, error_instance):
    """
    Validates the equality of two elements. In case of a difference
    between them, raises a given exception instance
    :param received: received element
    :param expected: expected element
    :param error_instance: exception instance to be raised in case of invalidity
    :return: raises an exception if the value is not valid
    """
    if received != expected:
        raise error_instance
