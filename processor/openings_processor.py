"""
File contains payload processor
"""
from constants import CLOSE, OPEN, WEEK_DAYS
from processor.data_structures import Day, TimeFrame, Week
from processor.exceptions import OpenTimeProcessorError
from processor.openings_validator import validate_openings
from processor_helpers import get_generator_with_remain


class OpenTimeProcessor(object):
    """
    processes weekly schedule and generates a Week instance.
    """

    def _get_collection_day(self, day_instance, current_day_name, day_list):
        """
        Auxiliary method to update the day from the collection processor.
        Generates a new day instance when the collection day is different from
        the collection day. In addition, it append the collection day to the
        list of days.
        :param day_instance: day being collected
        :param current_day_name: name of the day to be collected
        :param day_list: list of collected days
        :return: day instance to be collected
        """
        if day_instance:
            if day_instance.name != current_day_name:
                # new day to be collected let's append the current and create a
                # new one
                day_list.append(day_instance)
                day = Day(current_day_name)
            else:
                # keep collecting the same day
                day = day_instance
        else:
            # no day was initialized
            day = Day(current_day_name)
        return day

    def _is_close_day_valid(self, open_day, close_entry):
        """
        Check if close day is the same or the next day. Otherwise, raises an
        OpenTimeProcessorError
        :param open_day: instance of the open day
        :param close_entry: entry of the close day (dictionary)
        :return: raises an exception if close day is not valid
        """
        open_day_name = open_day.name
        close_day_name = close_entry['day']

        index_open_day = WEEK_DAYS.index(open_day_name)
        index_close_day = WEEK_DAYS.index(close_day_name)
        index_next_day = (index_open_day + 1) % (len(WEEK_DAYS))

        if index_close_day != index_open_day and \
                index_close_day != index_next_day:
            raise OpenTimeProcessorError("Close day is greater than next day.")

    def _add_time_frame_to_day(self, day, hour_entry, time_frame):
        """
        Check if the close day is valid and adds it to the time frame.
        A valid close date is a
        :param day: day instance
        :param hour_entry: dictionary with day, value and type
        :param time_frame: time frame to add close
        """
        self._is_close_day_valid(day, hour_entry)
        time_frame.set_close_time(hour_entry)
        day.add_open_time_frame(time_frame)

    def _get_list_open_days(self, hour_generator, remain):
        """
        From the hour generator generates the list of days containing the
        respective time frames.
        :param hour_generator: hour generator
        :param remain: initial close entry (in case there's one)
        :return: a list of Day instances
        """
        time_frame = None
        day = None
        week_days_list = []
        for hour_entry in hour_generator:

            if hour_entry['type'] == OPEN and not time_frame:
                day = self._get_collection_day(day, hour_entry['day'],
                                               week_days_list)
                time_frame = TimeFrame(hour_entry)

            elif time_frame and hour_entry['type'] == CLOSE:
                self._add_time_frame_to_day(day, hour_entry, time_frame)
                time_frame = None
            else:
                raise OpenTimeProcessorError("Processing Exception")

        if remain:
            self._add_time_frame_to_day(day, remain, time_frame)

        if time_frame and not time_frame.complete:
            raise OpenTimeProcessorError("Time not complete")

        week_days_list.append(day)

        return week_days_list

    def process(self, weekly_schedule):
        """
        Populates the data structures with the given weekly schedule.
        :param weekly_schedule: dictionary with week days in the keys. Contains
        nested dictionaries in each value with the open/close hours.
        :return: week instance
        """
        validate_openings(weekly_schedule)

        hour_generator, remain = get_generator_with_remain(weekly_schedule)

        week_days_list = self._get_list_open_days(
            hour_generator, remain)

        return Week(week_days_list)


if __name__ == '__main__':
    from json import load
    with open('../fixtures/unsorted_payload_example.json') as f:
        pdf_example = load(f)
    processor_instance = OpenTimeProcessor()
    week = processor_instance.process(pdf_example)
    print week
