from processor.exceptions import (
    OpenTimeProcessorValidationError,
    OpenTimeProcessorError
)
from processor.openings_processor import OpenTimeProcessor

from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)


class OpeningProcessorApi(Resource):

    def __init__(self):
        super(OpeningProcessorApi, self).__init__()
        self.open_time_processor = OpenTimeProcessor()

    def post(self):
        payload = request.get_json()

        try:
            week = self.open_time_processor.process(payload)
            response = {'message': str(week)}
        except OpenTimeProcessorError as e:
            return {'message': 'Error while processing: %s' % str(e)}
        except OpenTimeProcessorValidationError as e:
            return {'message': 'Validation error: %s' % str(e)}
        except Exception as e:
            return {'message': 'Unexpected exception: %s' % str(e)}

        return response


api.add_resource(OpeningProcessorApi, '/')

if __name__ == '__main__':
    app.run(debug=True)

